package alex.com.br.desafioconcrete.common;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by alexsoaresdesiqueira on 20/12/2017.
 */

public abstract class InternetConnection {

    public static boolean internetAvailable(Context context){
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
