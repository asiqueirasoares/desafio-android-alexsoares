package alex.com.br.desafioconcrete.request.modelService;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alexsoaresdesiqueira on 19/12/2017.
 */

public class RepositoryServiceModel {

    @SerializedName("items")
    private List<RepositoryServiceItemsModel> repositoryServiceItemsModelList;

    public RepositoryServiceModel(List<RepositoryServiceItemsModel> repositoryServiceItemsModelList) {
        this.repositoryServiceItemsModelList = repositoryServiceItemsModelList;
    }

    public RepositoryServiceModel() {
    }

    public List<RepositoryServiceItemsModel> getRepositoryServiceItemsModelList() {
        return repositoryServiceItemsModelList;
    }

    public void setRepositoryServiceItemsModelList(List<RepositoryServiceItemsModel> repositoryServiceItemsModelList) {
        this.repositoryServiceItemsModelList = repositoryServiceItemsModelList;
    }
}
