package alex.com.br.desafioconcrete.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import alex.com.br.desafioconcrete.adapter.binding.ProgressViewBiding;

/**
 * Created by alexsoaresdesiqueira on 20/12/2017.
 */

public class ProgressViewHolder extends RecyclerView.ViewHolder {

    private ProgressViewBiding progressViewBiding;

    public ProgressViewHolder(View itemView) {
        super(itemView);
        progressViewBiding = (ProgressViewBiding) itemView;
    }

    public ProgressViewBiding getIProgressViewBiding() {
        return progressViewBiding;
    }

}
