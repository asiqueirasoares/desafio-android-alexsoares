package alex.com.br.desafioconcrete.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import alex.com.br.desafioconcrete.adapter.binding.ItemViewBinding;

/**
 * Created by alexsoaresdesiqueira on 18/12/2017.
 */

public class ItemViewHolder extends RecyclerView.ViewHolder{

    private ItemViewBinding itemViewBinding;

    public ItemViewHolder(View itemView) {
        super(itemView);
        itemViewBinding = (ItemViewBinding) itemView;
    }

    public ItemViewBinding getItemViewBinding() {
        return itemViewBinding;
    }
}
