package alex.com.br.desafioconcrete.request;

import java.util.List;

import alex.com.br.desafioconcrete.request.modelService.PullRequestServiceModel;
import alex.com.br.desafioconcrete.request.modelService.RepositoryServiceModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by alexsoaresdesiqueira on 19/12/2017.
 */

public interface GitServices {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<RepositoryServiceModel> getRepositorys(@Query("page") String page);

    @GET("repos/{creator}/{repo}/pulls")
    Call<List<PullRequestServiceModel>> getPullRequestList(@Path("creator") String creator, @Path("repo") String repo);
}
