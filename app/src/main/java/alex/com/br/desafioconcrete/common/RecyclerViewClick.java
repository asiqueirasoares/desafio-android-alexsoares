package alex.com.br.desafioconcrete.common;

/**
 * Created by alexsoaresdesiqueira on 19/12/2017.
 */

public interface RecyclerViewClick {
    void onItemClick(Object object);
}
