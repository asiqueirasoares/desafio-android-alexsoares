package alex.com.br.desafioconcrete.common;

/**
 * Created by alexsoaresdesiqueira on 20/12/2017.
 */

public interface RecyclerViewLoadMoreItems {

    void onLoadMoreItems(Object object);
}
