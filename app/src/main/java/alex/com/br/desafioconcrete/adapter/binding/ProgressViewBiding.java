package alex.com.br.desafioconcrete.adapter.binding;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import alex.com.br.desafioconcrete.R;
import alex.com.br.desafioconcrete.common.InternetConnection;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by alexsoaresdesiqueira on 20/12/2017.
 */

@EViewGroup(R.layout.item_loading)
public class ProgressViewBiding extends ConstraintLayout {

    @ViewById
    ProgressBar progressBar;

    @ViewById
    TextView tvInfo;

    public ProgressViewBiding(Context context) {
        super(context);
    }

    public void indeterminateProgress(boolean indeterminate){
        progressBar.setIndeterminate(indeterminate);
    }
}
